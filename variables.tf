variable "project" {
  default = "selectchat"
}

variable "db_name"{
 default = "db"
}

variable "credentials" {
  default="selectchat.json"
}

variable "location" {
  default = "us-west1"
}

variable "region" {
  default = "us-west1"
}

variable "zone" {
  default = "us-west1-a"
}

variable "dns_zone" {
  default="uat.select-chat.com"
}

variable "cluster" {
  default = {
    name="uat"
    network = "uat-network"
    service_account = "uat-cluster-sa"
    master_cidr = "172.16.1.16/28"
    default_machine_type = "n1-standard-2"
    zones=[
      "us-west1-a",
      "us-west1-b",
      "us-west1-c"
    ]
    subnets = [
          {
            name    = "uat-subnet-1"
            cidr = "10.2.0.0/17"
          },
          {
            name    = "uat-subnet-2"
            cidr = "10.2.128.0/17"
          },
        ]
      } 
}

variable "db_password" {
  default = ""
}
variable "p81" {
  default = {
    peer = ""
    secret = ""
  }
}
variable "p81_all" {
  default = {
    peer = ""
    secret = ""
  }
}
