  module "dns-zone" {
    source  = "terraform-google-modules/cloud-dns/google"
    version = "2.0.0"
    project_id = "selectchat"
    type       = "public"
    name       = "${var.cluster.name}-select-chat-com"
    domain     = "${var.cluster.name}.select-chat.com."
  }
