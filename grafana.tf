# resource "kubernetes_config_map" "grafana-dashboards" {
#   metadata {
#     name = "grafana-dashboards"
#     namespace = "monitoring"
#   }

#   data = {
#     "grafana-dashboards"= "${file("helm/grafana/dashboards")}"
#   }
# }

resource "null_resource" "grafana-dashboards" {
  provisioner "local-exec" {
    command = "kubectl -n monitoring create configmap grafana-dashboards --from-file=helm/grafana/dashboards/"
  }
  depends_on = [module.gke_cluster.endpoint]
}

resource "helm_release" "grafana" {
  name          = "grafana"
  namespace     = "monitoring"
  repository    = "${data.helm_repository.stable.metadata.0.name}"  
  chart         = "grafana"
  version       = "4.3.2"
  #long <pending> for LoadBalancer  
  timeout       = 3600
  values = [
    "${file("${local.helm_values_path}/grafana.yaml")}"
  ]
  set {
    name  = "ingress.hosts[0]"
    value = "grafana.${var.cluster.name}.select-chat.com"
  }
  set {
    name  = "ingress.tls[0].secretName"
    value = "grafana-tls"
  }
  set {
    name = "ingress.tls[0].hosts[0]"
    value="grafana.${var.cluster.name}.select-chat.com"
  }
  depends_on = [module.gke_cluster.endpoint]
}
