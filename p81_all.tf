resource "google_compute_vpn_tunnel" "tunnel_p81_all" {
  name          = "${var.cluster.name}-tunnel-p81-all"
  peer_ip       = "${var.p81_all.peer}"
  shared_secret = "${var.p81_all.secret}"
  local_traffic_selector = ["0.0.0.0/0"]
  remote_traffic_selector = ["0.0.0.0/0"]
  target_vpn_gateway = google_compute_vpn_gateway.target_gateway_p81_all.self_link

  depends_on = [
    google_compute_forwarding_rule.fr_esp_p81_all,
    google_compute_forwarding_rule.fr_udp500_p81_all,
    google_compute_forwarding_rule.fr_udp4500_p81_all,
  ]
}

resource "google_compute_vpn_gateway" "target_gateway_p81_all" {
  name    = "${var.cluster.name}-p81-all-gw"
  network = module.vpc_network.network
}

resource "google_compute_address" "vpn_static_ip_p81_all" {
  name = "${var.cluster.name}-vpn-gw-static-ip-p81-all"
}

resource "google_compute_forwarding_rule" "fr_esp_p81_all" {
  name        = "${var.cluster.name}-p81-all-fr-esp"
  ip_protocol = "ESP"
  ip_address  = google_compute_address.vpn_static_ip_p81_all.address
  target      = google_compute_vpn_gateway.target_gateway_p81_all.self_link
}

resource "google_compute_forwarding_rule" "fr_udp500_p81_all" {
  name        = "${var.cluster.name}-p81-all-fr-udp500-p81-all"
  ip_protocol = "UDP"
  port_range  = "500"
  ip_address  = google_compute_address.vpn_static_ip_p81_all.address
  target      = google_compute_vpn_gateway.target_gateway_p81_all.self_link
}

resource "google_compute_forwarding_rule" "fr_udp4500_p81_all" {
  name        = "${var.cluster.name}-p81-all-fr-udp4500-p81-all"
  ip_protocol = "UDP"
  port_range  = "4500"
  ip_address  = google_compute_address.vpn_static_ip_p81_all.address
  target      = google_compute_vpn_gateway.target_gateway_p81_all.self_link
}

resource "google_compute_route" "route-p81-all" {
  name       = "${var.cluster.name}-p81-route-p81-all"
  network    = module.vpc_network.network
  dest_range = "10.252.0.0/16"
  priority   = 1000

  next_hop_vpn_tunnel = google_compute_vpn_tunnel.tunnel_p81_all.self_link
}