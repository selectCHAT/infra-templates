data "helm_repository" "stable" {
  name = "stable"
  url  = "https://kubernetes-charts.storage.googleapis.com"
}

data "helm_repository" "jetstack" {
  name = "jetstack"
  url  = "https://charts.jetstack.io"
}

data "helm_repository" "kiwigrid" {
  name = "kiwigrid"
  url  = "https://kiwigrid.github.io"
}

resource "kubernetes_service_account" "tiller" {
  metadata {
    name      = "terraform-tiller"
    namespace = "kube-system"
  }

  automount_service_account_token = true
  depends_on = [module.gke_cluster.endpoint]
}

resource "kubernetes_cluster_role_binding" "tiller" {
  metadata {
    name = "terraform-tiller"
  }

  role_ref {
    kind      = "ClusterRole"
    name      = "cluster-admin"
    api_group = "rbac.authorization.k8s.io"
  }

  subject {
    kind = "ServiceAccount"
    name = "terraform-tiller"

    api_group = ""
    namespace = "${kubernetes_service_account.tiller.metadata.0.namespace}"
  }
}

provider "helm" {

  install_tiller  = true
  tiller_image    = "gcr.io/kubernetes-helm/tiller:v2.16.1"
  service_account = "${kubernetes_service_account.tiller.metadata.0.name}"
  namespace       = "${kubernetes_service_account.tiller.metadata.0.namespace}"
  max_history     = 32

  # override        = ["spec.template.spec.tolerations[0].key=node-role.kubernetes.io/master","spec.template.spec.tolerations[0].effect=NoSchedule"]

  kubernetes {
    host                   = "${module.gke_cluster.endpoint}"
    # token                  = "${module.gke_cluster.access_token}"
    client_certificate     = "${module.gke_cluster.client_certificate}"
    client_key             = "${module.gke_cluster.client_key}"
    cluster_ca_certificate = "${module.gke_cluster.cluster_ca_certificate}"
#    load_config_file       = false
  }
}

