locals {
  cert_manager_sa_roles = concat(
    ["roles/dns.admin"]
  )
}

resource "google_service_account" "cert-manager-sa" {
  account_id   = "${var.cluster.name}-cert-manager-sa"
  display_name = "${var.cluster.name} cert manager service account"
}

resource "google_project_iam_member" "cert_manager_sa_roles" {
  for_each = toset(local.cert_manager_sa_roles)
  project = var.project
  role    = each.value
  member  = "serviceAccount:${google_service_account.cert-manager-sa.email}"
}


resource "google_service_account_key" "cert-manager-sa-key" {
  service_account_id = google_service_account.cert-manager-sa.name
}

resource "kubernetes_secret" "cert-manager-credentials" {
  metadata {
    name = "cert-manager-credentials"
    namespace = "cert-manager"
  }
  data = {
    "credentials.json" = base64decode(google_service_account_key.cert-manager-sa-key.private_key)
  }
  depends_on = [ module.gke_cluster.endpoint]
}

resource "null_resource" "cert-manager-crds" {
  provisioner "local-exec"{
    command = "kubectl apply -n cert-manager --validate=false -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.12/deploy/manifests/00-crds.yaml"
  }
  depends_on = [module.gke_cluster.endpoint]
}


resource "helm_release" "cert-manager" {
  name          = "cert-manager"
  namespace     = "cert-manager"
  repository = "https://charts.jetstack.io"
  chart         = "cert-manager"
  version       = "v0.12.0"
  disable_webhooks = true
  values = [
    "${file("${local.helm_values_path}/cert-manager.yaml")}"
  ]
  depends_on = [module.gke_cluster.endpoint]
  # set {
    # name  = "cluster.enabled"
    # value = "true"
  # }
}

resource "null_resource" "cert-manager-production-issuer" {
  provisioner "local-exec"{
    command = "kubectl apply -n cert-manager -f helm/cert-manager/production_issuer.yaml"
  }
  depends_on = [module.gke_cluster.endpoint]
}
resource "null_resource" "cert-manager-staging-issuer" {
  provisioner "local-exec"{
    command = "kubectl apply -n cert-manager -f helm/cert-manager/staging_issuer.yaml"
  }
  depends_on = [module.gke_cluster.endpoint]
}
