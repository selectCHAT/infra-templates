resource "google_compute_vpn_tunnel" "p81-tunnel" {
  name          = "${var.cluster.name}-p81-tunnel"
  peer_ip       = "${var.p81.peer}"
  shared_secret = "${var.p81.secret}"
  local_traffic_selector = ["0.0.0.0/0"]
  remote_traffic_selector = ["0.0.0.0/0"]
  target_vpn_gateway = google_compute_vpn_gateway.target_gateway.self_link

  depends_on = [
    google_compute_forwarding_rule.fr_esp,
    google_compute_forwarding_rule.fr_udp500,
    google_compute_forwarding_rule.fr_udp4500,
  ]
}

resource "google_compute_vpn_gateway" "target_gateway" {
  name    = "${var.cluster.name}-p81-gw"
  network = module.vpc_network.network
}

resource "google_compute_address" "vpn_static_ip" {
  name = "${var.cluster.name}-vpn-gw-static-ip"
}

resource "google_compute_forwarding_rule" "fr_esp" {
  name        = "${var.cluster.name}-p81-fr-esp"
  ip_protocol = "ESP"
  ip_address  = google_compute_address.vpn_static_ip.address
  target      = google_compute_vpn_gateway.target_gateway.self_link
}

resource "google_compute_forwarding_rule" "fr_udp500" {
  name        = "${var.cluster.name}-p81-fr-udp500"
  ip_protocol = "UDP"
  port_range  = "500"
  ip_address  = google_compute_address.vpn_static_ip.address
  target      = google_compute_vpn_gateway.target_gateway.self_link
}

resource "google_compute_forwarding_rule" "fr_udp4500" {
  name        = "${var.cluster.name}-p81-fr-udp4500"
  ip_protocol = "UDP"
  port_range  = "4500"
  ip_address  = google_compute_address.vpn_static_ip.address
  target      = google_compute_vpn_gateway.target_gateway.self_link
}

resource "google_compute_route" "route1" {
  name       = "${var.cluster.name}-p81-route"
  network    = module.vpc_network.network
  dest_range = "10.254.0.0/16"
  priority   = 1000

  next_hop_vpn_tunnel = google_compute_vpn_tunnel.p81-tunnel.self_link
}