resource "helm_release" "elasticsearch" {
  name          = "elasticsearch"
  namespace     = "logging"
  repository    = "${data.helm_repository.stable.metadata.0.name}"
  chart         = "elasticsearch"
  version       = "1.32.2"

  values = [
    "${file("${local.helm_values_path}/elasticsearch.yaml")}"
  ]
  depends_on = [module.gke_cluster.endpoint]
}

resource "helm_release" "elasticsearch_exporter" {
  name          = "elasticsearch-exporter"
  namespace     = "logging"
  repository    = "${data.helm_repository.stable.metadata.0.name}"
  chart         = "elasticsearch-exporter"
  version       = "2.2.0"

  values = [
    "${file("${local.helm_values_path}/elasticsearch-exporter.yaml")}"
  ]
  depends_on = [module.gke_cluster.endpoint]
}

resource "helm_release" "elasticsearch_curator" {
  name          = "elasticsearch-curator"
  namespace     = "logging"
  repository    = "${data.helm_repository.stable.metadata.0.name}"
  chart         = "elasticsearch-curator"
  version       = "2.1.3"

  values = [
    "${file("${local.helm_values_path}/elasticsearch-curator.yaml")}"
  ]
  depends_on = [module.gke_cluster.endpoint]
}

resource "helm_release" "fluentd" {
  name          = "fluentd"
  namespace     = "logging"
  repository    = "${data.helm_repository.kiwigrid.metadata.0.name}"
  chart         = "fluentd-elasticsearch"
  version       = "2.3.2"

  values = [
    "${file("${local.helm_values_path}/fluentd-elasticsearch.yaml")}"
  ]
  depends_on = [module.gke_cluster.endpoint]
}

resource "helm_release" "elastalert" {
  count         = 0
  name          = "elastalert"
  namespace     = "logging"
  repository    = "${data.helm_repository.stable.metadata.0.name}"
  chart         = "elastalert"
  version       = "1.2.3"

  values = [
    "${file("${local.helm_values_path}/elastalert.yaml")}"
  ]
  depends_on = [module.gke_cluster.endpoint]
}
