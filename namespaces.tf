resource "kubernetes_namespace" "cert-manager" {
  metadata {
    name = "cert-manager"
      labels = {
        "certmanager.k8s.io/disable-validation" = "true"
    }
  }
  depends_on = [module.gke_cluster.endpoint]
}
resource "kubernetes_namespace" "logging" {
  metadata {
    name = "logging"
    
  }
  depends_on = [module.gke_cluster.endpoint]
}

resource "kubernetes_namespace" "monitoring" {
  metadata {
    name = "monitoring"
  }
  depends_on = [module.gke_cluster.endpoint]
}

resource "kubernetes_namespace" "nginx-ingress" {
  metadata {
    name = "nginx-ingress"
  }
  depends_on = [module.gke_cluster.endpoint]
}
