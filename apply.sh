#!/bin/sh
if [ -z $1 ];then
    echo "Usage appy.sh <dev|uat|prod>"
    exit
fi
cp backend.tf.$1 backend.tf
rm .terraform/terraform.tfstate
terraform init --var-file=vars/$1.tfvars.json
terraform apply --var-file=vars/$1.tfvars.json