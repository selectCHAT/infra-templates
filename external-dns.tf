locals {
  all_service_account_roles = concat(
    ["roles/dns.admin"]
  )
}

resource "google_service_account" "external-dns-sa" {
  account_id   = "${var.cluster.name}-external-dns-sa"
  display_name = "external dns service account"
}

resource "google_project_iam_member" "service_account-roles" {
  for_each = toset(local.all_service_account_roles)
  project = var.project
  role    = each.value
  member  = "serviceAccount:${google_service_account.external-dns-sa.email}"
}


resource "google_service_account_key" "external-dns-sa-key" {
  service_account_id = google_service_account.external-dns-sa.name
}

resource "kubernetes_secret" "external-dns-sa-credentials" {
  metadata {
    name = "external-dns-sa-credentials"
    namespace = "kube-system"
  }
  data = {
    "credentials.json" = base64decode(google_service_account_key.external-dns-sa-key.private_key)
  }
}

resource "helm_release" "external-dns" {
  name          = "external-dns"
  namespace     = "kube-system"
  repository    = "${data.helm_repository.stable.metadata.0.name}"
  chart         = "external-dns"
  version       = "2.13.3"

  values = [
    "${file("${local.helm_values_path}/external-dns.yaml")}"
  ]
  set {
    name = "domainFilters" 
    value = "{${var.cluster.name}.select-chat.com}"
  }
}
