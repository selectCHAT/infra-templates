variable "dns_zone" {
  default="uat.select-chat.com"
}

variable "cluster" {
    name="uat"
    network = "uat-network"
    service_account = "uat-cluster-sa"
    master_cidr = "172.16.1.16/28"
    default_machine_type = "n1-standard-2"
    zones=[
      "us-west1-a",
      "us-west1-b",
      "us-west1-c"
    ]
    subnets = [
          {
            name    = "uat-subnet-1"
            cidr = "10.2.0.0/17"
          },
          {
            name    = "uat-subnet-2"
            cidr = "10.2.128.0/17"
          },
        ]
}
