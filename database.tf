
resource "google_compute_global_address" "private_ip_address" {
  provider = google-beta

  name          = "${var.cluster.name}-db-private-ip"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = module.vpc_network.network
}

resource "google_service_networking_connection" "private_vpc_connection" {
  provider = google-beta

  network                 = module.vpc_network.network
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_address.name]
}


resource "google_sql_database_instance" "instance" {
  provider = google-beta
  database_version = "SQLSERVER_2017_STANDARD"
  region = "us-west1"
  name = var.db_name
  depends_on = [google_service_networking_connection.private_vpc_connection]
  root_password = var.db_password

  settings {
    tier = "db-custom-1-4096"
    ip_configuration {
      ipv4_enabled    = false
      private_network = module.vpc_network.network
    }
  }
}
