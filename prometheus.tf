resource "helm_release" "prometheus" {
  name          = "prometheus"
  namespace     = "monitoring"
  repository    = "${data.helm_repository.stable.metadata.0.name}"
  chart         = "prometheus"
  version       = "10.3.1"

  values = [
    "${file("${local.helm_values_path}/prometheus.yaml")}"
  ]
}
