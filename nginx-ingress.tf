resource "helm_release" "nginx-ingress" {
  name          = "nginx-ingress"
  namespace     = "nginx-ingress"
  repository    = "${data.helm_repository.stable.metadata.0.name}"
  chart         = "nginx-ingress"
  version       = "1.29.1"

  values = [
    "${file("${local.helm_values_path}/nginx-ingress.yaml")}"
  ]
}
