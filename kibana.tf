resource "helm_release" "kibana" {
  name          = "kibana"
  namespace     = "logging"
  repository    = "${data.helm_repository.stable.metadata.0.name}"
  chart         = "kibana"
  version       = "3.2.5"

  values = [
    "${file("${local.helm_values_path}/kibana.yaml")}"
  ]
  set {
    name  = "ingress.tls[0].secretName"
    value = "kibana-tls"
  }
  set {
    name = "ingress.tls[0].hosts[0]"
    value="kibana.${var.cluster.name}.select-chat.com"
  }
  set {
    name = "ingress.hosts[0]"
    value="kibana.${var.cluster.name}.select-chat.com"
  }
}
